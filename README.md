# README #

This repository collects all efforts to get acquainted with wikidata, mygeneinfo, and the like.

### helloGeneInfo.java ###

Collects and entry from myGeneInfo and converts it into RDF to load in a SPARQL endpoint. Linking identifier to http://identifiers.org allows harvesting of other linked data resources (e.g. gene-disease links (http://www.disgenet.org/web/DisGeNET/v2.1;jsessionid=cx5hv3p8y1nio9rghecydvww) expression atlas (https://www.ebi.ac.uk/fgpt/atlasrdf/sparql)

The current script loads the JSON available on "http://mygene.info/v2/gene/ENSG00000123374". This json output is converted into RDF and loaded in a SPARQL endpoint. 

related identifiers are available with the following sparql query:

    select distinct * FROM <http://mygene.info/v2/gene/> where 
    {
       ?s rdfs:seeAlso <http://identifiers.org/uniprot/P24941> .
       ?s rdfs:seeAlso ?otherIdentifiers .
    }
[Results](http://95.85.25.210:8890/sparql?default-graph-uri=&query=select+distinct+*+FROM+%3Chttp%3A%2F%2Fmygene.info%2Fv2%2Fgene%2F%3E+where+%0D%0A%7B%0D%0A++%3Fs+rdfs%3AseeAlso+%3Chttp%3A%2F%2Fidentifiers.org%2Funiprot%2FP24941%3E+.%0D%0A++%3Fs+rdfs%3AseeAlso+%3FotherIdentifiers+.%0D%0A%7D+&should-sponge=&format=text%2Fhtml&CXML_redir_for_subjs=121&CXML_redir_for_hrefs=&timeout=0&debug=on)
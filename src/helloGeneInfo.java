import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.ReifiedStatement;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.DC;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;







public class helloGeneInfo {

	/**
	 * @param args
	 * @throws IOException 
	 */

	public static void addRdfsSeeAlso(Model model, Resource myGeneInfoUri, String source, String identifier){
		Resource seeAlso = model.createResource(source+identifier);
		Resource seeAlsoSource = model.createResource(source);
		myGeneInfoUri.addProperty(RDFS.seeAlso, seeAlso);
		seeAlso.addProperty(DCTerms.isPartOf, seeAlsoSource);
	}
	
	public static void addNestedRdfsSeeAlso(Model model, Resource myGeneInfoResource, JsonObject jsonObject, String key, String source){
		if (jsonObject.get(key).isJsonArray()){
			for (int i = 0; i < jsonObject.get(key).getAsJsonArray().size(); i++){
				System.out.println(key+": "+ jsonObject.get(key).getAsJsonArray().get(i).getAsString());
				String identifier = jsonObject.get(key).getAsJsonArray().get(i).getAsString();
				addRdfsSeeAlso(model, myGeneInfoResource, source, identifier) ;	
			}
		}
		else {
			System.out.println(key+": "+jsonObject.get(key).getAsString());
			String identifier = jsonObject.get(key).getAsString();
			addRdfsSeeAlso(model, myGeneInfoResource, source, identifier) ;		
		}
	}
	
	

	public static void main(String[] args) throws IOException {
		//Create jena model to fill with RDF
		
		Model model = ModelFactory.createDefaultModel();
		Resource myGeneInfoResource = model.createResource("http://mygene.info/v2/gene/ENSG00000123374");
		Resource identifiersOrgResource = model.createResource("http://identifiers.org/ensembl/ENSG00000123374");
		myGeneInfoResource.addProperty(DC.identifier, identifiersOrgResource);
		
		// details to get mygene info data
		URL url = new URL("http://mygene.info/v2/gene/ENSG00000123374");
		InputStream is = url.openStream();
		String jstring = IOUtils.toString(is, "UTF-8");
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonObject myGeneInfoObject = parser.parse(jstring).getAsJsonObject();
		
		//Get the data
		
		//accession
		JsonObject accessionObject = myGeneInfoObject.get("accession").getAsJsonObject();
		//Accession Genomic
		addNestedRdfsSeeAlso(model, myGeneInfoResource, accessionObject, "genomic", "http://identifiers.org/ena.embl/");
		
		//Accession Protein
		addNestedRdfsSeeAlso(model, myGeneInfoResource, accessionObject, "protein", "http://identifiers.org/ncbiprotein/");
		
		//Accession RNA
		addNestedRdfsSeeAlso(model, myGeneInfoResource, accessionObject, "rna", "http://identifiers.org/ena.embl/");
		
		//alias
		//TODO Create proper class
		if (myGeneInfoObject.get("alias").isJsonArray()){
			for (int i = 0; i < myGeneInfoObject.get("alias").getAsJsonArray().size(); i++){
				System.out.println("Alias: "+ myGeneInfoObject.get("alias").getAsJsonArray().get(i).getAsString());
				myGeneInfoResource.addLiteral(Skos.altLabel, myGeneInfoObject.get("alias").getAsJsonArray().get(i).getAsString());
			}
		}
		else {
			System.out.println(myGeneInfoObject.get("alias").getAsString());
			myGeneInfoResource.addLiteral(Skos.altLabel, myGeneInfoObject.get("alias").getAsString());
		}
		
		//ec
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "ec", "http://identifiers.org/enzyme/");
		
		//Ensembl
		JsonObject ensemblObject = myGeneInfoObject.get("ensembl").getAsJsonObject();
		
		addNestedRdfsSeeAlso(model, myGeneInfoResource, ensemblObject, "gene", "http://identifiers.org/ensembl/");
		addNestedRdfsSeeAlso(model, myGeneInfoResource, ensemblObject, "protein", "http://identifiers.org/ensembl/");
		addNestedRdfsSeeAlso(model, myGeneInfoResource, ensemblObject, "transcript", "http://identifiers.org/ensembl/");
		
		//EntrezGene
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "entrezgene", "http://identifiers.org/ncbigene/");
		
		//exons 
		//TODO exons
		
		//TODO generif
		
		//TODO genomic pos
		
		//TODO go
		
		//hgnc
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "HGNC", "http://identifiers.org/hgnc/");
		
		//TODO homologene
		
		//hprd
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "HPRD", "http://identifiers.org/hgnc/");
		
		//TODO interpro
		
		//ipi
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "ipi", "http://identifiers.org/ipi/");
		
		//TODO map_location
		
		//mim
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "MIM", "http://identifiers.org/mim/");
		
		//TODO name
		
		//TODO pathway
		
		//pdb
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "pdb", "http://identifiers.org/pdb/");
		
		//pharmakb
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "pharmgkb", "http://identifiers.org/pharmgkb/");
		model.createResource("http://identifiers.org/pharmgkb/").addProperty(RDF.type, "NOT SPECIFIED");
		
		//pir
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "pir", "http://identifiers.org/pir/");
		model.createResource("http://identifiers.org/pir/").addProperty(RDF.type, "NOT SPECIFIED");
		
		//prosite
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "prosite", "http://identifiers.org/prosite/");
		
		//TODO reagent
		
		//refseq
		JsonObject refseqObject = myGeneInfoObject.get("refseq").getAsJsonObject();
		//refseq genomic
		addNestedRdfsSeeAlso(model, myGeneInfoResource, refseqObject, "genomic", "http://identifiers.org/refseq/");
		addNestedRdfsSeeAlso(model, myGeneInfoResource, refseqObject, "rna", "http://identifiers.org/refseq/");
		addNestedRdfsSeeAlso(model, myGeneInfoResource, refseqObject, "protein", "http://identifiers.org/refseq/");
		
		//TODO reporter
		//TODO summary
		
		//TODO "symbol": "CDK2",
		//TODO "taxid": 9606,
		//TODO "type_of_gene": "protein-coding",
		//unigene
		addNestedRdfsSeeAlso(model, myGeneInfoResource, myGeneInfoObject, "unigene", "http://identifiers.org/unigene");

		//uniprot
		JsonObject uniprotObject = myGeneInfoObject.get("uniprot").getAsJsonObject();
		addNestedRdfsSeeAlso(model, myGeneInfoResource, uniprotObject, "Swiss-Prot", "http://identifiers.org/uniprot/");
		addNestedRdfsSeeAlso(model, myGeneInfoResource, uniprotObject, "TrEMBL", "http://identifiers.org/uniprot/");
		
		//TODO "Vega": "OTTHUMG00000170575"ry
		
		
		FileOutputStream file = new FileOutputStream(new File("/tmp/myGeneInfo.ttl"));
		model.write(file, "TURTLE");
		/* MygeneInfo[] obj2 = gson.fromJson(inputJsonString, MygeneInfo[].class);   
		 System.out.println(inputJsonString);
		 System.out.println(obj2[0]);


		/*    ArrayList<channelSearchEnum> lcs = new ArrayList<channelSearchEnum>();

		    for(JsonElement obj : jArray )
		    {
		        channelSearchEnum cse = gson.fromJson( obj , channelSearchEnum.class);
		        lcs.add(cse);
		    }
		 */
	}

}
